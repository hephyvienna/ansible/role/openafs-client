# openafs-client [![Build Status](https://travis-ci.com/hephyvienna/ansible-role-openafs-server.svg?branch=master)](https://travis-ci.com/hephyvienna/ansible-role-openafs-server) [![Ansible Role](https://img.shields.io/ansible/role/40973.svg)](https://galaxy.ansible.com/hephyvienna/openafs_client)


Configure an OpenAFS client, including integrated login with _pam_afs_session_

## Requirements

*   EL6/7
*   For _pam_afs_session_ EPEL is required
*   The host should have joined the Kerberos REALM.


## Role Variables

    openafs_client_cell: openafs.org

Add your cell name. This parameter is required.

   openafs_client_copr_enabled: true

OpenAFS is not included in CentOS. It is required to add
the COPR repositories maintained by J. Billings.

    openafs_client_pkgs:
      - openafs-client
      - openafs-krb5

Default client packages to be installed.

    openafs_client_dkms: false

The kernel module included in the repository is not always matching the current kernel.
It may be required to use DKMS to build the kernel on demand.

    openafs_client_krb5_options:
      renew_lifetime: 120h
      forwardable: true
      proxiable: true


Modify the basic kerberos configuration to match the needs of
OpenAFS.


## Example Playbook

    - hosts: servers
      roles:
         - role: geerlingguy.repo-epel
         - role: hephyvienna.openafs-client
           vars:
             openafs_client_cell: hephy.at

# License

MIT

# Author Information

Written by [Dietrich Liko](Dietrich.Liko@oeaw.ac.at) in April 2019

[Institute for High Energy Physics](http://www.hephy.at) of the
[Austrian Academy of Sciences](http://www.oeaw.ac.at)
